#!/bin/sh

set -o nounset # all variables must be set

if [[ -e .env ]]
then
    echo "Sourcing variables from .env..."
    . ./.env
else
    echo "No .env file found"
fi

# fetch kube config from digitalocean
KUBECONFIG=$(mktemp)

curl --request GET \
  --url "https://api.digitalocean.com/v2/kubernetes/clusters/$DIGITAL_OCEAN_CLUSTER_ID/kubeconfig" \
  --header "authorization: Bearer $DIGITAL_OCEAN_TOKEN" > ${KUBECONFIG}

echo "Run helm update..."

# Helper script to deploy

KUBECONFIG=${KUBECONFIG} helm upgrade --install \
    --set service.shpeelyPrisma.secrets.prismaSecret=$SHPEELY_PRISMA_PRISMA_SECRET \
    --set service.shpeelyPrisma.secrets.prismaManagementApiSecret=$SHPEELY_PRISMA_PRISMA_MANAGEMENT_API_SECRET \
    --set service.shpeelyPrisma.secrets.auth0ClientSecret=$SHPEELY_PRISMA_AUTH0_CLIENT_SECRET \
    --set service.shpeelyPrisma.secrets.auth0ManagementSecret=$SHPEELY_PRISMA_AUTH0_MANAGEMENT_SECRET \
    --set service.shpeelyPrisma.secrets.mailgunApiKey=$SHPEELY_PRISMA_MAILGUN_API_KEY \
    --set service.postgres.secrets.password=$POSTGRES_PASSWORD \
    --set service.stats.secrets.awsSecretAccessKey=$AWS_SECRET_ACCESS_KEY \
    --set service.stats.secrets.awsAccessKeyId=$AWS_ACCESS_KEY_ID \
    shpeely .

