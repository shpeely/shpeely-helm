#!/bin/sh

set -x
set -o nounset # all variables must be set


if [[ -e .env ]]
then
    echo "Sourcing variables from .env..."
    . ./.env
else
    "No .env file found"
fi

echo "Run helm update..."

# Helper script to deploy

helm install --debug --dry-run \
    --set service.shpeelyPrisma.secrets.prismaSecret=$SHPEELY_PRISMA_PRISMA_SECRET \
    --set service.shpeelyPrisma.secrets.prismaManagementApiSecret=$SHPEELY_PRISMA_PRISMA_MANAGEMENT_API_SECRET \
    --set service.shpeelyPrisma.secrets.auth0ClientSecret=$SHPEELY_PRISMA_AUTH0_CLIENT_SECRET \
    --set service.shpeelyPrisma.secrets.auth0ManagementSecret=$SHPEELY_PRISMA_AUTH0_MANAGEMENT_SECRET \
    --set service.shpeelyPrisma.secrets.mailgunApiKey=$SHPEELY_PRISMA_MAILGUN_API_KEY \
    --set service.postgres.secrets.password=$POSTGRES_PASSWORD \
    .